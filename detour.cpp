BYTE* DetourFunction(BYTE* OrigByte, BYTE* HookByte, int MemLength) {
	const DWORD JMP = 0xE9;
	const DWORD NOP = 0x90;
	DWORD OldProt = NULL;
	BYTE* Reserved;
	int expansion = 0;
	for (int j = 0; j < MemLength; j++) {
		if (OrigByte[j] == 0xEB) {
			//for when short JMPs turn to near
			expansion++;
		}
	}
	expansion = MemLength + (expansion * 3) + 5;
	//Reserved = (BYTE*)malloc(expansion);
	Reserved = (BYTE*)VirtualAlloc(NULL, expansion, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	//PAGE_EXECUTE_READWRITE to allow DEP to be enabled
	VirtualProtect((LPVOID)OrigByte, MemLength, PAGE_EXECUTE_READWRITE, &OldProt);
	//For storing 
	int skip = 0;
	DWORD oldRelative;
	DWORD newRelative;
	//For temporarily storing short JMP data to overwrite later
	int* jmpLocation;
	DWORD* jmpAddress;
	jmpLocation = new int[(int)floor(MemLength / 2)];
	jmpAddress = new DWORD[(int)floor(MemLength / 2)];
	int jmpArrayNext = 0;
	//
	for (int i = 0; i < MemLength; i++) {
		if (skip == 0) {
			switch (OrigByte[i])
			{
			case 0xEB: //JMP short
				skip = 1;
				if ((OrigByte[i + 1] & 0xF0) >= 0x80) {
					//hexadecimal value is negative, extract in unsigned form
					oldRelative = (DWORD)(0x100 - OrigByte[i + 1]);
					newRelative = (DWORD)((OrigByte + 0x2) + i) - oldRelative;
				}
				else {
					//extract bytes
					oldRelative = (DWORD)OrigByte[i + 1];
					newRelative = (DWORD)((OrigByte + 0x2) + i) + oldRelative;
				}
				jmpLocation[jmpArrayNext] = i;
				jmpAddress[jmpArrayNext] = newRelative;
				jmpArrayNext++;
				break;
			case 0xE9: //JMP relative
				skip = 4;
				//determine if two's complement has been used (& 0xF0 is not needed but used for the sake of debugging)
				if ((OrigByte[i + 4] & 0xF0) >= 0x80) {
					//hexadecimal value is negative, extract in little endian unsigned form
					oldRelative = (DWORD)(((0xFF - OrigByte[i + 4]) << 24) + ((0xFF - OrigByte[i + 3]) << 16) + ((0xFF - OrigByte[i + 2]) << 8) + (0x100 - OrigByte[i + 1]));
					newRelative = (DWORD)((OrigByte + 0x5) + i) - oldRelative;
				}
				else {
					//extract bytes in little endian format
					oldRelative = (DWORD)((OrigByte[i + 4] << 24) + (OrigByte[i + 3] << 16) + (OrigByte[i + 2] << 8) + (OrigByte[i + 1]));
					newRelative = (DWORD)((OrigByte + 0x5) + i) + oldRelative;
				}
				*(DWORD*)((OrigByte + 1) + i) = newRelative - (DWORD)(Reserved + 0x5 + i);
				break;
			case 0xE8: //CALL
					   //same as JMP
				skip = 4;
				if ((OrigByte[i + 4] & 0xF0) >= 0x80) {
					oldRelative = (DWORD)(((0xFF - OrigByte[i + 4]) << 24) + ((0xFF - OrigByte[i + 3]) << 16) + ((0xFF - OrigByte[i + 2]) << 8) + (0x100 - OrigByte[i + 1]));
					newRelative = (DWORD)((OrigByte + 0x5) + i) - oldRelative;
				}
				else {
					oldRelative = (DWORD)((OrigByte[i + 4] << 24) + (OrigByte[i + 3] << 16) + (OrigByte[i + 2] << 8) + (OrigByte[i + 1]));
					newRelative = (DWORD)((OrigByte + 0x5) + i) + oldRelative;
				}
				*(DWORD*)((OrigByte + 1) + i) = newRelative - (DWORD)(Reserved + 0x5 + i);
				break;
			}
		}
		else {
			skip--;
		}
	}
	//
	memcpy(Reserved, OrigByte, MemLength);
	//
	int pushup = 0; //to keep track of how many locations we've pushed data forward
	if (jmpArrayNext > 0) {
		//turn short to near JMP
		jmpArrayNext = 0;
		for (int k = 0; k < expansion; k++) {
			if (jmpLocation[jmpArrayNext] == k) {
				memcpy(Reserved + ((k + pushup) + 5), Reserved + ((k + pushup) + 2), expansion - ((k + pushup) + 3)); //Copies memory down so 3 more bytes can be used for JMP
				Reserved[k + pushup] = JMP; //setting to relative near jump
				*(DWORD*)((Reserved + 1) + (k + pushup)) = jmpAddress[jmpArrayNext] - (DWORD)(Reserved + 0x5 + (k + pushup)); //Stores relative JMP
				pushup += 3;
				jmpArrayNext++;
			}
		}
	}
	//
	Reserved += (expansion - 5);
	Reserved[0] = JMP;
	*(DWORD*)(Reserved + 1) = (DWORD)((OrigByte + MemLength) - (Reserved + 5));
	memset(OrigByte, NOP, MemLength); //Clean up our code we messed around with
	OrigByte[0] = JMP;
	*(DWORD*)(OrigByte + 1) = (DWORD)(HookByte - (OrigByte + 5));
	VirtualProtect((LPVOID)OrigByte, MemLength, OldProt, NULL);
	//
	delete[] jmpAddress;
	delete[] jmpLocation;
	//
	BYTE* returnval;
	returnval = Reserved - (expansion - 5); //Calculate start of reserved space to return
	VirtualProtect((LPVOID)returnval, expansion, PAGE_EXECUTE_READ, NULL); //Revoke write privileges for protection
	return (returnval);
}